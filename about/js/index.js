

$(function(){

	setSlider($("#ComeSakuranomiyaBlanc .slider li"),$("#ComeSakuranomiyaBlanc .slider ul"));
	setSlider($("#ComeSakuranomiyaNoir .slider li"),$("#ComeSakuranomiyaNoir .slider ul"));
	setSlider($("#ComeMiyakojimaHondori .slider li"),$("#ComeMiyakojimaHondori .slider ul"));
	
});

/*===================================================
						Window
=====================================================*/

//ページが読み込まれたら実行1
$(window).load(function() {	

});

//ウィンドウリサイズしたら実行
$(window).resize(function() {

});

//スクロールしたら実行
$(window).scroll(function() {
});


function setSlider(thumbs,wrap){
	var clone_str = "clone";
	var animateClass = "moveLeft";
	var wrap_width = Math.floor(wrap.width());
	var thumbs_length = thumbs.length;
	var thumbs_width = Math.floor(wrap_width/thumbs_length);
	wrap.width(wrap_width*2);
	thumbs.width(thumbs_width);
	thumbs.clone(true).appendTo(wrap).addClass(clone_str);
	wrap.addClass(animateClass);
	//ウィンドウリサイズしたら実行
	$(window).resize(function() {
	if(device != "sp"){
		wrap_width = wrap.parent().width();
		thumbs_width = Math.floor(wrap_width/thumbs_length);
		wrap.width(wrap_width*2);
		thumbs.width(thumbs_width);
		wrap.find(".clone").remove();
		thumbs.clone(true).appendTo(wrap).addClass(clone_str);
	}
	});
	
}