
/*===================================================
						変数
=====================================================*/

var contactType;

/*===================================================
						実行
=====================================================*/

$(function(){

	 $.fn.autoKana('#sei', '#sei-kana', {
        katakana : true
    });
    $.fn.autoKana('#mei', '#mei-kana', {
        katakana : true
    });
	//フォームチェックで表示
	$('.request-show').hide();
	$('.survey-show').hide();
	$('.contact-show').hide();

	$('input[name="contact"]:radio').click(function() {
		$('.request-show').hide();
		$('.survey-show').hide();
		$('.contact-show').hide();
		var val = $(this).val();
		switch(val){
			case "1":
				$('.contact-show').show();
			break;
			case "2":
				$('.survey-show').show();
			break;
			case "3":
				$('.request-show').show();
			break;
		}
	});

	var url = location.href;
//	contactType = location.search.match(/type=(.*?)(&|$)/)[1];
	
	var match = location.search.match(/type=(.*?)(&|$)/);
	if(match) {
		contactType = decodeURIComponent(match[1]);
		
		switch(contactType){
		case "contact":
			$('.contact-show').show();
			$("input[name='contact']").prop('checked',false);
			$("input[name='contact'][value='"+"1"+"']").prop('checked',true);
		break;
		case "survey":
			$('.survey-show').show();
			$("input[name='contact']").prop('checked',false);
			$("input[name='contact'][value='"+"2"+"']").prop('checked',true);
		break;
		case "request":
			$('.request-show').show();
			$("input[name='contact']").prop('checked',false);
			$("input[name='contact'][value='"+"3"+"']").prop('checked',true);
		break;


	}
	}


});


/*===================================================
						Window
=====================================================*/
// TODO 追加分
//ページが読み込まれたら実行1
$(window).load(function () {

	var val = document.form_index;
	if(val){
		$('.request-show').hide();
		$('.survey-show').hide();
		$('.contact-show').hide();

		switch(val.contact.value){
			case "1":
				$('.contact-show').show();
			break;
			case "2":
				$('.survey-show').show();
			break;
			case "3":
				$('.request-show').show();
			break;
		}
	}

});

//ウィンドウリサイズしたら実行
$(window).resize(function() {

});

//スクロールしたら実行
$(window).scroll(function() {
});



