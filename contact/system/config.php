<?php
ini_set('error_reporting', E_ALL | ~E_STRICT);

require_once(dirname(__FILE__).'/lib/common.php');
require_once(dirname(__FILE__).'/lib/validation.php');
require_once(dirname(__FILE__).'/lib/sendmail.php');

//入力画面テンプレート
define("FORM_INPUT", dirname(__FILE__)."/tmpl/index.php");

//確認画面テンプレート
define("FORM_CONFIRM", dirname(__FILE__)."/tmpl/confirm.php");

//管理者メールアドレス
//define("ADMIN_MAIL_ADDRESS", "anno@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS", "okada@invogue.co.jp");
define("ADMIN_MAIL_ADDRESS", "yoshimura@forus-and.co.jp");
define("ADMIN_MAIL_ADDRESS2", "info@forus-and.co.jp");
define("ADMIN_MAIL_ADDRESS3", "kawaguchi@forus-and.co.jp");

// お問い合わせ種別
$type_select = array(
		1 => 'お問合せ',
		2 => '無料調査',
		3 => '資料請求',
);

//バリデーションクラスインスタンス生成
$VALIDATION = new validation();

//メールクラスインスタンス生成
$MAIL = new sendmail();