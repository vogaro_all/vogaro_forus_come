<!DOCTYPE html>
<html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" class="">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>お問い合わせ・無料調査・資料請求｜COME｜戸建賃貸・土地活用</title>
<meta name="description" content="サイトの説明文50文字程度" />
<meta name="keywords" content="COME,カム,土地活用,戸建賃貸" />

<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/images/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/images/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/images/manifest.json">
<link rel="mask-icon" href="/images/safari-pinned-tab.svg" color="#535f6d">
<meta name="apple-mobile-web-app-title" content="COME 戸建賃貸">
<meta name="application-name" content="COME 戸建賃貸">
<meta name="theme-color" content="#ffffff">

<meta property="og:type" content="website">
<meta property="og:url" content="https://www.come-forus.com/">
<meta property="og:site_name" content="COME｜戸建賃貸・土地活用">
<meta property="og:title" content="COME">
<meta property="og:description" content="土地活用に今注目度の高い「戸建て賃貸」。フォーラス&カンパニーでは戸建て賃貸事業（COME）を展開しております。土地活用と賃貸の在り方をイノベーションし、物件に付加価値をつけてオンリーワンの土地活用をご提供致します。">
<meta property="og:image" content="https://www.come-forus.com/ogp.jpg">

<meta name="viewport" content="width=device-width,initial-scale=0.5,maximum-scale=2,minimum-scale=1,user-scalable=yes">
<meta name="format-detection" content="telephone=no,address=no,email=no">
<!--[if lt IE 9]><script src="..js/html5shiv.js"></script><![endif]-->

<link href="https://fonts.googleapis.com/earlyaccess/notosansjapanese.css" rel="stylesheet" />
<link rel="stylesheet" href="../css/global.css?1" media="all">
<link rel="stylesheet" href="../css/common.css" media="all">
<link rel="stylesheet" href="../css/lower.css" media="all">
<link rel="stylesheet" href="css/contact.css" media="all">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KTGVQZV');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KTGVQZV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--<div id="Loading">
	<div><img src="images/logo.png" alt="戸建賃貸COME" width="150" /></div>
</div>-->
<div id="Page" class="home">
<!-- Header
*****************************************-->
<header id="Header">
<div id="HeaderInner">
<p id="Title"><a href="/"><img src="../images/logo.png" alt="戸建賃貸COME"/></a></p>
<nav id="Nav">
<ul id="GlobalNav">
<li><a href="/">HOME</a></li>
<li><a href="/about/">COMEについて</a></li>
<li><a href="/performance/">COMEの実績</a></li>
<li><a href="/company/">会社概要</a></li>
<li><a href="/contact/?type=contact">お問い合わせ</a></li>
</ul>
<ul id="ConversionNav">
<li><a href="/contact/?type=request"><span>資料請求</span></a></li>
<li><a href="/contact/?type=survey"><span>無料調査</span></a></li>
</ul>
</nav>
<div class="menu"><img src="../images/menu.gif" alt=""/></div>
</div><!--/ #HeaderInner-->
</header><!--/ #Header-->

<!-- Keyvisual
*****************************************-->
<section id="Keyvisual"><div class="section-inner">
<div class="perse"><img src="../images/kv-bg-lower.gif" alt=""/></div>
<h1><img src="images/title.png" class="contact-show" alt="CONTACT"/><img class="survey-show" src="images/survey-title.png" alt="SURVEY"/><img class="request-show" src="../contact/images/request-title.png" alt="REQUEST"/>
<span class="contact-show">お問い合わせ</span><span class="request-show">資料請求</span><span class="survey-show">無料調査</span>
</h1>
</div><!--/ .section-inner--></section><!--/ #Keyvisual -->

<!-- Contents
*****************************************-->
<div id="Contents">
<div id="ContentsInner">
<div class="txt">
<p>お問い合わせメールフォームです。下記事項をご記入の上、送信ください。<br>
<span class="required">必須</span>項目は必ずご入力ください。</p>
</div>

<!-- About
*****************************************-->
<section id="Contact"><div class="section-inner">

<div class="form-frame">
<ol class="form-flow cf">
<li><span>送信完了</span></li>
<li><span>確認画面</span></li>
<li class="active"><span>入力画面</span></li>
</ol>
<form class="form" name="form_index" action="./index.php" method="post">
<div class="form-inner">
<table class="form-tbl">
<tr>
<th><label class="from-label"><span class="required" >必須</span>お問合せ種目</label></th>
<td>
<?php if(!empty($err['contact'])): ?>
<p class="m-formList-error"><?php echo $err['contact']; ?></p>
<?php endif; ?>
<ul>
<li><label class="radio-label"><input type="radio" name="contact" value="1" <?php echo (!empty($data_list['contact']) && $data_list['contact'] == 1) ? 'checked':'';?> ><p>お問合せ</p></label></li>
<li><label class="radio-label"><input type="radio" name="contact" value="2" <?php echo (!empty($data_list['contact']) && $data_list['contact'] == 2) ? 'checked':'';?>><p>無料調査</p></label></li>
<li><label class="radio-label"><input type="radio" name="contact" value="3" <?php echo (!empty($data_list['contact']) && $data_list['contact'] == 3) ? 'checked':'';?>><p>資料請求</p></label></li>
</ul>
</td>
</tr>
<tr>
<th><label class="from-label"><span class="required">必須</span>お名前</label></th>
<td>
<?php if(!empty($err['name'])): ?>
<p class="m-formList-error"><?php echo $err['name']; ?></p>
<?php endif; ?>
<label><input type="text" id="name" name="name" class="input-base form-txt mr20" placeholder="例）鈴木太郎" value="<?php echo (!empty($data_list['name'])) ? $data_list['name']:'';?>"></label>
</td>
</tr>
<tr>
<th><span class="any">任意</span><label class="from-label">ふりがな</label></th>
<td>
<?php if(!empty($err['name-kana'])): ?>
<p class="m-formList-error"><?php echo $err['name-kana']; ?></p>
<?php endif; ?>
<label><input type="text" id="sei-kana" name="name-kana" class="input-base form-txt mr20" placeholder="例）ふりがな" value="<?php echo (!empty($data_list['name-kana'])) ? $data_list['name-kana']:'';?>"></label>
</td>
</tr>
<tr>
<th><span class="required">必須</span><label class="from-label">電話番号</label></th>
<td>
<?php if(!empty($err['tel'])): ?>
<p class="m-formList-error"><?php echo $err['tel']; ?></p>
<?php endif; ?>
<label><input type="text" id="tel" name="tel" class="input-base form-txt mr20" placeholder="例）06-1234-5678" value="<?php echo (!empty($data_list['tel'])) ? $data_list['tel']:'';?>"></label>
</td>
</tr>
<tr class="survey-show">
<th><span class="required">必須</span><label class="from-label">無料調査ご住所</label></th>
<td>
<?php if(!empty($err['survey-address'])): ?>
<p class="m-formList-error"><?php echo $err['survey-address']; ?></p>
<?php endif; ?>
<label><textarea id="survey-address" name="survey-address" class="input-base form-txt mr20" placeholder="例）大阪府大阪市北区天神橋2丁目2-11"><?php echo (!empty($data_list['survey-address'])) ? $data_list['survey-address']:'';?></textarea></label>
</td>
</tr>
<tr class="request-show">
<th><span class="required">必須</span><label class="from-label">資料送付先ご住所</label></th>
<td>
<?php if(!empty($err['postcode'])): ?>
<p class="m-formList-error"><?php echo $err['postcode']; ?></p>
<?php endif; ?>
<?php if(!empty($err['region'])): ?>
<p class="m-formList-error"><?php echo $err['region']; ?></p>
<?php endif; ?>
<?php if(!empty($err['local'])): ?>
<p class="m-formList-error"><?php echo $err['local']; ?></p>
<?php endif; ?>
<?php if(!empty($err['street'])): ?>
<p class="m-formList-error"><?php echo $err['street']; ?></p>
<?php endif; ?>
<input type="tel" id="postcode" name="postcode" class="input-base form-txt-s" value="<?php echo (!empty($data_list['postcode'])) ? $data_list['postcode']:'';?>" maxlength="7" placeholder="郵便番号（ハイフンなし）">
<input type="button" class="btn-zip" name="btn-zip" value="住所自動入力" onclick="AjaxZip3.zip2addr('postcode','','region','local','street','extended');">
<input type="text" name="region" class="input-base form-txt mr20" value="<?php echo (!empty($data_list['region'])) ? $data_list['region']:'';?>" placeholder="都道府県">
<input type="text" name="local" class="input-base form-txt mr20" value="<?php echo (!empty($data_list['local'])) ? $data_list['local']:'';?>" placeholder="市区町村">
<input type="text" name="street" class="input-base form-txt mr20" value="<?php echo (!empty($data_list['street'])) ? $data_list['street']:'';?>" placeholder="丁目番地">
<input type="text" name="extended" class="input-base form-txt mr20" value="<?php echo (!empty($data_list['extended'])) ? $data_list['extended']:'';?>" placeholder="建物名・部屋番号">
</td>
</tr>

<tr>
<th><label class="from-label"><span class="any">任意</span>メールアドレス</label></th>
<td>
<?php if(!empty($err['email'])): ?>
<p class="m-formList-error"><?php echo $err['email']; ?></p>
<?php endif; ?>
<label><input type="text" name="email" class="input-base form-txt-l" value="<?php echo (!empty($data_list['email'])) ? $data_list['email']:'';?>" placeholder="例） info@come.com"></label>
</td>
</tr>
<tr>
<th><label class="from-label"><span class="any">任意</span>確認用メールアドレス</label></th>
<td>
<?php if(!empty($err['email_confirm'])): ?>
<p class="m-formList-error"><?php echo $err['email_confirm']; ?></p>
<?php endif; ?>
<label><input type="text" name="email_confirm" class="input-base form-txt-l" value="<?php echo (!empty($data_list['email_confirm'])) ? $data_list['email_confirm']:'';?>" placeholder="※確認のため再度ご入力ください。"></label>
</td>
</tr>

<tr>
<th class="v-align-top"><span class="any">任意</span><label class="from-label">お問合せ内容</label></th>
<td>
<?php if(!empty($err['ask'])): ?>
<p class="m-formList-error"><?php echo $err['ask']; ?></p>
<?php endif; ?>
<textarea type="text" name="ask" class="input-base form-txt-l" value="" placeholder="お問い合わせ内容をご入力ください。"><?php echo (!empty($data_list['ask'])) ? $data_list['ask']:'';?></textarea>
</td>
</tr>
<!-- .form-tbl // --></table>
<!-- .form-inner // --></div>

<div class="privacy">
<h3>個人情報の利用目的</h3>
<ul>
	<li><strong>1. 個人情報の利用目的</strong><br>
お寄せいただいたお問い合わせに対する回答、連絡、管理のために利用いたします。</li>
<li><strong>2. 個人情報を第三者へ提供することについて</strong><br>
当社はお預かりした個人情報を第三者へ提供する場合は、JIS Q 15001:2006が要求する必要事項を、本人通知し、同意いただかない限り実施いたしません。但し、以下の場合は同意を得ず提供することがございます。<br>
法令に基づく場合<br>
人の生命、身体又は財産保護のために緊急に必要がある場合であって、本人の同意を得ることが困難な場合<br>
公衆衛生上の向上又は児童の健全な育成の推進のために特に必要がある場合であって、本人の同意を得ることが困難な場合<br>
国の機関若しくは地方公共団体又はその委託を受けた者が法令の定める事務を遂行することに対して協力する必要があって、本人の同意を得ることにより当該事務の遂行に支障を及ぼすおそれがある場合</li>
<li><strong>3. 個人情報の委託について</strong><br>
当社は一部業務(給与計算、社会保険手続き業務)を会計事務所および社会保険労務士事務所へ委託して行っております。当社と会計事務所及び社会保険労務士事務所は『機密情報に関する覚書』を取り交わしたうえで業務を委託しております。</li>
<li><strong>4. 個人情報の開示等の問い合わせについて</strong><br>
お預かりした個人情報の開示、利用目的の通知、訂正、追加又は削除、利用又は提供の拒否については、以下問い合わせ先までご一報ください。その際、ご登録いただいた情報をもとに本人確認をさせていただきます。</li>
<li><strong>5. 個人情報の任意性について</strong><br>
必須項目をご入力いただけない場合は、お問い合わせができないことがございますのでご了承ください。</li>
<li><strong>6. お問い合わせ先</strong><br>
株式会社フォーラス&amp;カンパニー<br>
〒530-0041<br>
大阪市北区天神橋2-2-11　阪急産業南森町ビル3階<br>
個人情報保護管理責任者　担当：吉村 貴成<br>
TEL：06-6882-1777<br>
FAX：06-6882-1770<br>
E-mail：yoshimura@forus-and.co.jp<br>
受付時間：10:00～18:00　年末年始は除く</li>
</ul>

</div>
</div><!--/ .section-inner--></section><!--/ #About -->
<div class="btn">
<div class="form-submit">
<button type="submit" class="btn-submit">送信内容を確認する</button>
<!-- .form-submit // --></div>
</div>
<input type="hidden" name="input" value="1">
<!-- .form // --></form>
<!-- .form-frame // --></div>

<!-- Phone
*****************************************-->
<div id="Phone">
<section class="phonenamber">
<ul>
<li class="txt">お電話でのお問い合わせ</li>
<li class="pc-show"><img src="images/icn-phone.png" alt=""/>06-6882-1777</li>
<li class="sp-show"><a href="tel:06-6882-1777"><img src="images/icn-phone.png" alt=""/>06-6882-1777</a></li>
<li class="time">受付時間：10:00～18:00<br>（年末年始は除く）</li>
</ul>
</section><!--/ .phonenamber -->
</div><!-- /#Phone -->


<!-- conversion
*****************************************-->
<section class="conversion view">
	<div class="survey"><div class="inner">
		<h2>土地の売買、土地活用をお考えの方</h2>
		<p class="lead">土地の調査、市場調査など今なら無料</p>
		<div class="btn"><a href="/contact/?type=survey">無料調査はこちら</a></div>
	</div></div>
	<div class="request"><div class="inner">
		<h2>今なら資料請求してくれた方全員に</h2>
		<p class="lead">COMEカタログプレゼント</p>
		<div class="btn"><a href="/contact/?type=request">資料請求はこちら</a></div>
	</div></div>
</section><!--/ .conversion -->

</div><!--/ #ContentsInner -->
</div><!--/ #Contents -->

<!-- Footer
*****************************************-->
<footer id="Footer">
<div id="FooterInner">
<div class="company">
<h2><img src="../images/logo-forusandcompany.png" alt="株式会社フォーラス&カンパニー"/></h2>
<p>株式会社フォーラス&amp;カンパニー<br>
大阪市北区天神橋2-2-11 阪急産業南森町ビル3階</p>
</div>
<nav>
<ul>
<li><a href="/company/">会社概要</a></li>
<li><a href="#">お問い合わせ</a></li>
</ul>
</nav>
<small>COPYRIGHT &copy; FORUS &amp; COMPANY, ALL RIGHTS RESERVED.</small>
</div>
</footer>

</div><!--/ #Page -->
<script src="../js/jquery1.8.3.pack.js"></script>
<script data-pace-options='{ "ajax": true }' src='../js/pace.js'></script>
<script src="../js/jquery.tile.js"></script>
<script src="../js/jquery.inview.js"></script>
<script src="../js/common.js"></script>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<script src="../js/jquery.autoKana.js"></script>
<script src="js/contact.js"></script>
</body>
</html>