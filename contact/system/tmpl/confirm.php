<!DOCTYPE html>
<html lang="ja" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" class="">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>確認画面｜お問い合わせ｜COME｜戸建賃貸・土地活用</title>
<meta name="description" content="サイトの説明文50文字程度" />
<meta name="keywords" content="COME,カム,土地活用,戸建賃貸" />

<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/images/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/images/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/images/manifest.json">
<link rel="mask-icon" href="/images/safari-pinned-tab.svg" color="#535f6d">
<meta name="apple-mobile-web-app-title" content="COME 戸建賃貸">
<meta name="application-name" content="COME 戸建賃貸">
<meta name="theme-color" content="#ffffff">

<meta property="og:type" content="website">
<meta property="og:url" content="https://www.come-forus.com/">
<meta property="og:site_name" content="COME｜戸建賃貸・土地活用">
<meta property="og:title" content="COME">
<meta property="og:description" content="土地活用に今注目度の高い「戸建て賃貸」。フォーラス&カンパニーでは戸建て賃貸事業（COME）を展開しております。土地活用と賃貸の在り方をイノベーションし、物件に付加価値をつけてオンリーワンの土地活用をご提供致します。">
<meta property="og:image" content="https://www.come-forus.com/ogp.jpg">

<meta name="viewport" content="width=device-width,initial-scale=0.5,maximum-scale=2,minimum-scale=1,user-scalable=yes">
<meta name="format-detection" content="telephone=no,address=no,email=no">
<!--[if lt IE 9]><script src="..js/html5shiv.js"></script><![endif]-->

<link href="https://fonts.googleapis.com/earlyaccess/notosansjapanese.css" rel="stylesheet" />
<link rel="stylesheet" href="../css/global.css?1" media="all">
<link rel="stylesheet" href="../css/common.css" media="all">
<link rel="stylesheet" href="../css/lower.css" media="all">
<link rel="stylesheet" href="css/confirm.css" media="all">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KTGVQZV');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KTGVQZV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--<div id="Loading">
	<div><img src="images/logo.png" alt="戸建賃貸COME" width="150" /></div>
</div>-->
<div id="Page" class="home">
<!-- Header
*****************************************-->
<header id="Header">
<div id="HeaderInner">
<p id="Title"><a href="/"><img src="../images/logo.png" alt="戸建賃貸COME"/></a></p>
<nav id="Nav">
<ul id="GlobalNav">
<li><a href="/">HOME</a></li>
<li><a href="/about/">COMEについて</a></li>
<li><a href="/performance/">COMEの実績</a></li>
<li><a href="/company/">会社概要</a></li>
<li><a href="/contact/?type=contact">お問い合わせ</a></li>
</ul>
<ul id="ConversionNav">
<li><a href="/contact/?type=request"><span>資料請求</span></a></li>
<li><a href="/contact/?type=survey"><span>無料調査</span></a></li>
</ul>
</nav>
<div class="menu"><img src="../images/menu.gif" alt=""/></div>
</div><!--/ #HeaderInner-->
</header><!--/ #Header-->

<!-- Keyvisual
*****************************************-->
<section id="Keyvisual"><div class="section-inner">
<div class="perse"><img src="../images/kv-bg-lower.gif" alt=""/></div>
<h1><img src="images/title.png" class="contact-show" alt="CONTACT"/><img class="survey-show" src="images/survey-title.png" alt="SURVEY"/><img class="request-show" src="../contact/images/request-title.png" alt="REQUEST"/>
<span class="contact-show">お問い合わせ</span><span class="request-show">資料請求</span><span class="survey-show">無料調査</span>
</h1>
</div><!--/ .section-inner--></section><!--/ #Keyvisual -->

<!-- Contents
*****************************************-->
<div id="Contents">
<div id="ContentsInner">
<div class="txt">
<p>下記の内容をご確認の上、送信してください。</p>
</div>

<!-- About
*****************************************-->
<section id="Contact"><div class="section-inner">

<div class="form-frame">
<ol class="form-flow cf">
<li><span>送信完了</span></li>
<li class="active"><span>確認画面</span></li>
<li><span>入力画面</span></li>
</ol>
<form class="form" name="form_index" action="./index.php" method="post">
<div class="form-inner">
<table class="form-tbl">
<tr>
<th><label class="from-label"><span class="required" >必須</span>お問合せ種目</label></th>
<td>
<?php echo (!empty($data_list['contact'])) ? $type_select[$data_list['contact']]:'';?>
</td>
</tr>
<tr>
<th><label class="from-label"><span class="required">必須</span>お名前</label></th>
<td>
<label>
<?php echo (!empty($data_list['name'])) ? $data_list['name']:'';?>
</label>
</td>
</tr>
<tr>
<th><span class="any">任意</span><label class="from-label">ふりがな</label></th>
<td>
<?php echo (!empty($data_list['name-kana'])) ? $data_list['name-kana']:'';?>
</td>
</tr>
<tr>
<th><span class="required">必須</span><label class="from-label">電話番号</label></th>
<td>
<?php echo (!empty($data_list['tel'])) ? $data_list['tel']:'';?>
</td>
</tr>

<?php if ($data_list['contact'] == "2") {?>
<tr>
<th><span class="required">必須</span><label class="from-label">無料調査ご住所</label></th>
<td>
<?php echo (!empty($data_list['survey-address'])) ? $data_list['survey-address']:'';?>
</td>
</tr>
<?php }?>

<?php if ($data_list['contact'] == "3") {?>
<tr>
<th><span class="required">必須</span><label class="from-label">資料送付先ご住所</label></th>
<td>
〒<?php echo (!empty($data_list['postcode'])) ? $data_list['postcode']:'';?>&nbsp;
<?php echo (!empty($data_list['region'])) ? $data_list['region']:'';?>
<?php echo (!empty($data_list['local'])) ? $data_list['local']:'';?>
<?php echo (!empty($data_list['street'])) ? $data_list['street']:'';?>&nbsp;
<?php echo (!empty($data_list['extended'])) ? $data_list['extended']:'';?>
</td>
</tr>
<?php }?>

<tr>
<th><label class="from-label"><span class="any">任意</span>メールアドレス</label></th>
<td>
<?php echo (!empty($data_list['email'])) ? $data_list['email']:'';?>
</td>
</tr>
<tr>
<th class="v-align-top"><span class="any">任意</span><label class="from-label">お問合せ内容</label></th>
<td>
<?php echo (!empty($data_list['ask'])) ? nl2br($data_list['ask']):'';?>
</td>
</tr>
<!-- .form-tbl // --></table>
<!-- .form-inner // --></div>

</div><!--/ .section-inner--></section><!--/ #About -->
<div class="btn">
<div class="form-submit">
<div class="btn-back"><a href="#" onclick="javascript:window.history.back(-1);return false;">入力画面へ戻る</a></div>
<button name="comp" type="submit" class="btn-submit">上記の内容で送信する</button>
<!-- .form-submit // --></div>
</div>
<input type="hidden" name="comp" value="1">
<?php foreach ($data_list as $key => $value) : ?>
<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>">
<?php endforeach; ?>
<!-- .form // --></form>
<!-- .form-frame // --></div>

<!-- conversion
*****************************************-->
<section class="conversion view">
	<div class="survey"><div class="inner">
		<h2>土地の売買、土地活用をお考えの方</h2>
		<p class="lead">土地の調査、市場調査など今なら無料</p>
		<div class="btn"><a href="/contact/?type=survey">無料調査はこちら</a></div>
	</div></div>
	<div class="request"><div class="inner">
		<h2>今なら資料請求してくれた方全員に</h2>
		<p class="lead">COMEカタログプレゼント</p>
		<div class="btn"><a href="/contact/?type=request">資料請求はこちら</a></div>
	</div></div>
</section><!--/ .conversion -->

</div><!--/ #ContentsInner -->
</div><!--/ #Contents -->

<!-- Footer
*****************************************-->
<footer id="Footer">
<div id="FooterInner">
<div class="company">
<h2><img src="../images/logo-forusandcompany.png" alt="株式会社フォーラス&カンパニー"/></h2>
<p>株式会社フォーラス&amp;カンパニー<br>
大阪市北区天神橋2-2-11 阪急産業南森町ビル3階</p>
</div>
<nav>
<ul>
<li><a href="/company/">会社概要</a></li>
<li><a href="#">お問い合わせ</a></li>
</ul>
</nav>
<small>COPYRIGHT &copy; FORUS &amp; COMPANY, ALL RIGHTS RESERVED.</small>
</div>
</footer>

</div><!--/ #Page -->
<script src="../js/jquery1.8.3.pack.js"></script>
<script data-pace-options='{ "ajax": true }' src='../js/pace.js'></script>
<script src="../js/jquery.tile.js"></script>
<script src="../js/jquery.inview.js"></script>
<script src="../js/common.js"></script>
<script src="https://ajaxzip3.github.io/ajaxzip3.js"></script>
<script src="../js/jquery.autoKana.js"></script>
<script src="js/contact.js"></script>
</body>
</html>