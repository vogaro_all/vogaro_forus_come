<?php
require_once(dirname(__FILE__).'/system/config.php');

error_reporting(0);

//完了画面へ遷移
if(!empty($_POST['comp'])){
	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);

	$complete_type = '?type=contact';
	$subject = "COMEのお問い合わせフォームよりメールの発信がありました。";

	$body = "以下の通り、ホームページより弊社に関するお問い合わせを承りました。\r\n";
	$body .= "お問い合わせ日時:".(date('Y年m月d日　H時i分'))."\r\n";
	$body .= "\r\n";
	$body .= "■お問合せ内容\r\n";
	$body .= "お問合せ種目：".$type_select[$data_list['contact']]."\r\n";
	$body .= "\r\n";
	$body .= "■お客さま情報\r\n";
	$body .= "────────────────────────────────\r\n";
	$body .= "[お名前]　:　".$data_list['name']."\r\n";
	$body .= "[ふりがな]　:　".$data_list['name-kana']."\r\n";
	$body .= "[電話番号]　: ".$data_list['tel']."\r\n";
	$body .= "[メールアドレス]　:　".$data_list['email']."\r\n";
	
	if ($data_list['contact'] == 2) {
		$body .= "[無料調査ご住所]　:　".$data_list['survey-address']."\r\n";
	
		$complete_type = '?type=survey';
	} else if ($data_list['contact'] == 3) {
		$body .= "[資料送付先ご住所]　:　";
		$body .= "〒".$data_list['postcode'];
		$body .= " ".$data_list['region'];
		$body .= $data_list['local'];
		$body .= $data_list['street'];
		$body .= " ".$data_list['extended']."\r\n";
	
		$complete_type = '?type=request';
	}
	
	$body .= "\r\n";
	$body .= "■お問い合わせ詳細\r\n";
	$body .= "────────────────────────────────\r\n";
	$body .= "[お問い合わせの内容]\r\n";
	$body .= "".$data_list['ask']."\r\n";
	$body .= "\r\n";
	$body .= "━╋━━━━━━━━━━━━━━━━━━…‥‥・・・‥…━╋━\r\n";
	$body .= " 株式会社フォーラス＆カンパニー\r\n";
	$body .= " 株式会社　トラストエージェント\r\n";
	$body .= "\r\n";
	$body .= "〒530-0041\r\n";
	$body .= "大阪市北区天神橋2-2-11　阪急産業南森町ビル3F\r\n";
	$body .= "TEL:06-6882-2112    FAX06-6882-2115\r\n";
	$body .= "トラストエージェント\r\n";
	$body .= "http://www.trustagent.co.jp/\r\n";
	$body .= "\r\n";
	$body .= "フォーラスアンドカンパニー\r\n";
	$body .= "http://www.forus-and.co.jp/\r\n";
	$body .= "営業時間: 9:30～19:00\r\n";
	$body .= "　　\r\n";
	$body .= "━╋━…‥・・・‥‥…━━━━━  Osaka minamimorimati ━╋━\r\n";

	//管理者宛メール
	$MAIL->send(ADMIN_MAIL_ADDRESS, $subject, $body, ADMIN_MAIL_ADDRESS);
	$MAIL->send(ADMIN_MAIL_ADDRESS2, $subject, $body, ADMIN_MAIL_ADDRESS2);
	$MAIL->send(ADMIN_MAIL_ADDRESS3, $subject, $body, ADMIN_MAIL_ADDRESS3);

	if (isset($data_list['email']) && !empty($data_list['email'])) {
	$subject = "【株式会社フォーラス＆カンパニー】お問い合わせありがとうございます。";

	$body = "".$data_list['name']." 様\r\n";
	$body .= "\r\n";
	$body .= "この度は戸建賃貸（COME）にお問い合わせ頂き誠にありがとうございました。\r\n";
	$body .= "内容を確認いたしまして、改めて担当者よりご連絡をさせていただきます。\r\n";
	$body .= "お問い合わせ日時:".(date('Y年m月d日　H時i分'))."\r\n";
	$body .= "\r\n";
	$body .= "■お問合せ内容\r\n";
	$body .= "お問合せ種目：".$type_select[$data_list['contact']]."\r\n";
	$body .= "\r\n";
	$body .= "■お客さま情報\r\n";
	$body .= "────────────────────────────────\r\n";
	$body .= "[お名前]　:　".$data_list['name']."\r\n";
	$body .= "[ふりがな]　:　".$data_list['name']."\r\n";
	$body .= "[電話番号]　: ".$data_list['tel']."\r\n";
	$body .= "[メールアドレス]　:　".$data_list['email']."\r\n";
	
	if ($data_list['contact'] == 2) {
		$body .= "[無料調査ご住所]　:　".$data_list['survey-address']."\r\n";
	} else if ($data_list['contact'] == 3) {
		$body .= "[資料送付先ご住所]　:　";
		$body .= "〒".$data_list['postcode'];
		$body .= " ".$data_list['region'];
		$body .= $data_list['local'];
		$body .= $data_list['street'];
		$body .= " ".$data_list['extended']."\r\n";
	}
	
	$body .= "\r\n";
	$body .= "■お問い合わせ詳細\r\n";
	$body .= "────────────────────────────────\r\n";
	$body .= "[お問い合わせの内容]\r\n";
	$body .= "".$data_list['ask']."\r\n";
	$body .= "────────────────────────────────\r\n";
	$body .= "※注意事項\r\n";
	$body .= "この度は、お問い合わせをお寄せいただきありがとうございます。\r\n";
	$body .= "このメールは、お問い合わせの受信完了をお知らせするために自動送信されたものです。\r\n";
	$body .= "（お寄せいただいたお問い合わせへの回答ではございません。）\r\n";
	$body .= "\r\n";
	$body .= "なお、以下の点につき、ご理解をお願いいたします。\r\n";
	$body .= "・詳細の確認が必要な場合など、いただいたお問い合わせの内容によっては返信に時間をいただくことがございます。\r\n";
	$body .= "・いただいたお問い合わせの内容によっては、回答いたしかねる場合がございます。\r\n";
	$body .= "・土曜日・日曜日・祝祭日、年末年始等におきましては、翌営業日以降の対応となります。\r\n";
	$body .= "今後とも株式会社フォーラス＆カンパニーをよろしくお願いいたします。\r\n";
	$body .= "━╋━━━━━━━━━━━━━━━━━━…‥‥・・・‥…━╋━\r\n";
	$body .= " 株式会社フォーラス＆カンパニー\r\n";
	$body .= " 株式会社　トラストエージェント\r\n";
	$body .= "\r\n";
	$body .= "〒530-0041\r\n";
	$body .= "大阪市北区天神橋2-2-11　阪急産業南森町ビル3F\r\n";
	$body .= "TEL:06-6882-2112    FAX06-6882-2115\r\n";
	$body .= "トラストエージェント\r\n";
	$body .= "http://www.trustagent.co.jp/\r\n";
	$body .= "\r\n";
	$body .= "フォーラスアンドカンパニー\r\n";
	$body .= "http://www.forus-and.co.jp/\r\n";
	$body .= "営業時間: 9:30～19:00\r\n";
	$body .= "\r\n";
	$body .= "━╋━…‥・・・‥‥…━━━━━  Osaka minamimorimati ━╋━\r\n";

	//関係者宛メール
	$MAIL->send($data_list['email'], $subject, $body, ADMIN_MAIL_ADDRESS);
	}

	header("Location: ./complete.html".$complete_type);
	exit;
}

//確認画面へ遷移
else if(!empty($_POST['input'])){

	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);

	//入力チェック
	$err_check_arr = array(
		"contact"=>array("name"=>"お問合せ種目","type"=>array("null","alpha_numeric")),
		"name"=>array("name"=>"お名前","type"=>array("null","len"),"length"=>255),
//		"name-kana"=>array("name"=>"ふりがな","type"=>array("null","len"),"length"=>255),
		"tel"=>array("name"=>"電話番号","type"=>array("null")),
//		"email"=>array("name"=>"メールアドレス","type"=>array("null","email","len"),"length"=>255),
//		"email_confirm"=>array("name"=>"確認用メールアドレス","type"=>array("null","email","len"),"length"=>255),
//		"ask"=>array("name"=>"お問い合わせ内容","type"=>array("null")),
	);
	if ($data_list['contact'] == 2) {
		$err_check_arr = array_merge($err_check_arr, array(
				"survey-address"=>array("name"=>"無料調査ご住所","type"=>array("null")),
		));
	} else if ($data_list['contact'] == 3) {
		$err_check_arr = array_merge($err_check_arr, array(
				"postcode"=>array("name"=>"郵便番号","type"=>array("null")),
				"region"=>array("name"=>"都道府県","type"=>array("null")),
				"local"=>array("name"=>"市区町村","type"=>array("null")),
				"street"=>array("name"=>"丁目番地","type"=>array("null")),
				//"extended"=>array("name"=>"建物名・部屋番号","type"=>array("null")),
		));
	}

	//バリデーション実行
	$err = $VALIDATION->check($data_list,$err_check_arr);

	//メールアドレスのバリデーション追加
	if(empty($err['email']) && empty($err['email_confirm'])){
		if($data_list['email'] != $data_list['email_confirm']){
			$err['email'] = $err['email_confirm'] = 'メールアドレスが一致しません。';
		}
	}

	if(empty($err)){
		require_once(FORM_CONFIRM);
	}else{
		require_once(FORM_INPUT);
	}
}

//入力画面へ戻る
else if(!empty($_POST['back'])){
	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);
	require_once(FORM_INPUT);
}

//入力画面
else{
	require_once(FORM_INPUT);
}