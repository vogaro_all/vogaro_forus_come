

$(function(){

	setSlider(
		$("#Vol1 .main li"),
		$("#Vol1 .main"),
		$("#Vol1 .thumbs")
	);

	setSlider(
		$("#Vol2 .main li"),
		$("#Vol2 .main"),
		$("#Vol2 .thumbs")
	);

	setSlider(
		$("#Vol3 .main li"),
		$("#Vol3 .main"),
		$("#Vol3 .thumbs")
	);

	setSlider(
		$("#Vol4 .main li"),
		$("#Vol4 .main"),
		$("#Vol4 .thumbs")
	);

	function setSlider(mains,wrap,thumbs){
		var show_count = 0;
		var clone_str = "clone";
		var wrap_width = wrap.width();
		var mains_length = mains.length;
		var mains_width = wrap_width;
		wrap.width(wrap_width*mains_length);
		mains.width(mains_width);
		var thumbs_clone = mains.clone(true).appendTo(thumbs);
		thumbs_clone.width("15%");
		wrap.animate({
			left: -(show_count * mains_width)
		});
		thumbs_clone.removeClass(active);
		thumbs_clone.eq(0).addClass(active);
		thumbs_clone.on('click', function() {
			var index = $(thumbs_clone).index(this);
			show_count = index;
			wrap.animate({
				left: -(show_count * mains_width)
			});
			thumbs_clone.removeClass(active);
			$(this).addClass(active);
		});
		$(window).resize(function() {
			if(device != "sp"){
				wrap_width = wrap.parent().width();
				mains_width = wrap_width;
				wrap.width(wrap_width*mains_length);
				mains.width(mains_width);
			}
			else {
				wrap_width = wrap.parent().width();
				mains_width = wrap_width;
				wrap.width(wrap_width*mains_length);
				mains.width(mains_width);
			}
		});
	}


	//アコーディオンメニュー
		$(".accordionNav").next().slideToggle(100);
		$(".accordionNav").on("click", function() {
			$(this).next().slideToggle();
			$(window).trigger('resize');
			//activeが存在する場合
			if ($(this).hasClass('active')) {
				// activeを削除
				$(this).removeClass('active');
			}
			else {
				// activeを追加
				$(this).addClass('active');
			}
		});
});

/*===================================================
						Window
=====================================================*/

//ページが読み込まれたら実行1
$(window).load(function() {

});

//ウィンドウリサイズしたら実行
$(window).resize(function() {

});

//スクロールしたら実行
$(window).scroll(function() {
});
