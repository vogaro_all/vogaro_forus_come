var bw;
var bh;
var device = "";
var active = "active";
var strHref = "href";
var speed = 800;
var Page;
var ease = "easeInOutCubic";
var spSize = 767;
var Nav;
var Menu;
var menuFlg = true;

$(function(){
	
	Page = $("#Page");
	Menu = $(".menu");
	Nav = $("#Nav");
	
	setStageSize();
	
	//メニュー展開
	$(Menu).on('click', function() {
		if(menuFlg){
			Nav.show();
			Nav.addClass(active);
			menuFlg =false;
		}else{
			Nav.hide();
			Nav.removeClass(active);
			menuFlg = true;
		}
	});
	
	$(Nav).on('click', function() {
		if(device=="sp"){
			if(menuFlg){
				Nav.show();
				Nav.addClass(active);
				menuFlg =false;
			}else{
				Nav.hide();
				Nav.removeClass(active);
				menuFlg = true;
			}
		}
	});
	
	//Inview
	$('.view').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass("move");
		}
	});
	
	//スムーススクロール
	$('a[href^=#]').click(function(){
		var href= $(this).attr(strHref);
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html,body").animate({scrollTop:position}, speed, ease);
		return false;
	});
	
	
});

/*===================================================
						Window
=====================================================*/

//ページが読み込まれたら実行1
$(window).load(function() {	
	Page.addClass(active);
});

//ウィンドウリサイズしたら実行
$(window).resize(function() {
	setStageSize();
});

//スクロールしたら実行
$(window).scroll(function() {
});

function setStageSize(){
	//Windowサイズの取得
	bw = window.innerWidth ? window.innerWidth : $(window).width();
	bh = window.innerHeight ? window.innerHeight : $(window).height();	
	//デバイス判定	
	if(bw <= spSize){
		device = "sp";
		menuFlg = true;
		Nav.hide();
		
	}else{
		device = "pc";
		Nav.show();
	}
}

