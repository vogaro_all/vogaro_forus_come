var Keyvisual;
var Solution;
var Solution_Panels;
var openFlg=true;

/*===================================================
						変数
=====================================================*/



/*===================================================
						実行
=====================================================*/

$(function(){

	Keyvisual = $("#Keyvisual");
	heightSet();
	
	Solution = $("#Solution");
	Solution_Panels = Solution.find("li");
	Solution_Panels.click(function(){
		Solution_Panels.removeClass(active);
		$(this).toggleClass(active);
	});
	
	setSlider($(".slider li"),$(".slider ul"));
	
});

function heightSet(){
	if(device!="sp" || openFlg != false){
		Keyvisual.height(bh);
		Keyvisual.find(".section-inner").height(bh);
		openFlg = false;
	}
}

/*===================================================
						Window
=====================================================*/

//ページが読み込まれたら実行1
$(window).load(function () {
	$(Keyvisual).find("span").addClass("anime");
	$(Keyvisual).find("span").on('webkitAnimationEnd', function(){
		$(this).addClass(active);
	});
	$(Keyvisual).find(".illust").on('webkitAnimationEnd', function(){
		$(this).addClass(active);
	});
});

//ウィンドウリサイズしたら実行
$(window).resize(function() {
	heightSet();
});

//スクロールしたら実行
$(window).scroll(function() {
});


function setSlider(thumbs,wrap){
	
	var animateClass = "moveLeft";
	var thumbs_length = thumbs.length;
	var thumbs_width = thumbs.width();
	var wrap_width = thumbs_width*thumbs_length;
	wrap.width(wrap_width);
	wrap.addClass(animateClass);
	
	
	
}

